# Improve Salsa CI

Week 2 (8-13 Jan) - Testing Debian Packages with sbuild and incorporating it into the initial Salsa CI pipeline
## 9.01.23
### To Do
- Create a test package and build it with ‘sbuild’

### Tasks Accomplished
- Create a sample package for test purposes
- Spent most of my time reading documentation and consuming information to get acquainted with sbuild and (s)chroot

### Blockages
- Was not successful with building the test package due to misconfiguration of the sbuild config files. Spent some time uninstalling and reinstalling sbuild and its dependencies until frustration

## 10.01.23 
### To Do
- Resolve Yesterday’s errors and successfully build the test package with sbuild

### Tasks Accomplished
- Successfully completed an overdue MR that was merged

### Blockages
- Could not find a suitable fix for the error I got as shown below
- Lost most of the day’s time running family-related errands

## 11.01.23 
### To Do
- Still figuring out the errors and making sure I build
- Incorporating ‘sbuild’ into the salsa pipeline

### Tasks Accomplished
- Successfully built the test package with ‘sbuild’ by;
- Defining the Distribution of the package in the changelog and creating its default configuration in the ‘/etc/schroot/schroot.conf’ configuration file
- After a successful build, I uploaded the test package to GitLab to build it against the Salsa CI pipeline to re-analyse the build process as shown below

### Findings so far
- I didn’t incorporate in some jobs that are in Salsa-CI in my sbuild tests
The lintian test fails in both builds even when I didn’t configure it manually in the sbuild meaning sbuild has some defaults that I need to find out

### Blockages
- Shallow knowledge of the sbuild configurations. I need to work on this so that I can add in the missing jobs

## 12.01.23 
### To Do
- Start incorporating ‘sbuild’ into salsa-ci.yml file from a separate branch
- Meeting my mentor(s) to explain some configuration concepts before I start to make changes

### Personal links (for reference)
- https://salsa.debian.org/salsa-ci-team/pipeline/-/blob/master/salsa-ci.yml#L472
- https://salsa.debian.org/salsa-ci-team/autopkgtest-lxc/

## 13.01.23
### To Do 
- Start creating a build script with sbuild
- I intend to start working on a build script from scratch in my sample package’s .gitlab-ci.yml file while following the .salsa-ci.yml build structure in the pipeline

### Tasks Accomplished
- Created a simple .gitlab-ci.yml file and run a few pipelines and got an error creating chroot.  The first reason was chroot package wasn’t installed. After installing it, a configuration file had to be configured. The configuration process seemed long and I decided to use sbuild-debian-developer-setup as a quick pass to successfully run sbuild. Unfortunately, I ran into an error while installing sbuild-debian-developer-setup

### Blockages
- On top of not successfully running the sbuild, there is frustration on having to build from scratch on every push which slows down the development time because the testing time for small and significant changes is the same

## 16.01.23
### To Do
- Resolving last week’s Friday errors to build successfully

## 17.01.23 - 20.01.23 
- there was a power outage for almost a whole week and it was so frustrating 😤 because I couldn't access the internet reliably and neither could I charge my devices 

## 23.01.23
### To Do
- Successfully make a build in the test repo.

### Tasks accomplished
- I didn’t make any progress. Same errors as previous times

### Blockages
- There was unstable power and couldn’t go so far

## 24.01.23 - 25.01.23
### Tasks
- Rebuilding the GitLab yml with and setting up sbuild with all options in this wiki

### Tasks accomplished
- Rerun chroot and sbuild tests to reproduce errors that we go through them meticulously with my mentor.
- Had a discussion with my mentor on the errors and how to proceed and also agreed on an approach to take 
The approaches included;
1. Going through the previous errors of the build jobs
2. Testing out on a  local environment

### Blockages
- There were no blockages so far. There were errors encountered and build failures but their intention was to help in digging into why the santiago-gsoc@debian.org‘sbuild’ could be failing in the pipeline

## 26.01.23 
### To Do
- Still focused on rebuilding and retesting stuff as I meet with my mentor more frequently

### Tasks accomplished
- I was able to use two methods of setting up sbuild. One was using sbuild debian developer setup two and the other was using the mmdebstrap. In both attempts, the major error was creating the (s)chroot. 
- I also run the same setup on another dummy package that was used by the Salsa-CI-Team for test purposes and got the same errors.

### Blockages
- After several attempts and running into the same errors. I got stuck on how to proceed with them hence need to do some more reading about the tools and also have more meetings and discussions with my mentor to find a way forward

29.01,23
### To Do 
- Restudying documentation

### Findings
- santiago-gsoc@debian.org do you know the meaning of this and how to achieve it? I was thinking it would be something to consider

## 30.01.23
### To Do
- Focus on setting up sbuild with s-d-d-setup and drop the other two ‘manual’ setups. 
This is because with ‘s-d-d-setup’, most of the processes are already set up and it is easier to run, also given the fact that the other tests like ‘lintian’ and ‘autopkgtests’ will be run separately, there’s no need to use the longer setups

### Tasks Accomplished
- I was able, with close collaboration with santiago-gsoc@debian.org, to make progress on the builds after finding solutions to the following blockages like This error was resolved by installing and running ‘s-d-d-setup with sudo.
 - I was excited to produce a different error (felt like progress to me). The error is probably caused by the docker container not being able to communicate with a particular network in order to pull the required Debian Mirror
#### Solution:
- Use ‘apt-cache-ng’ tool to create network

### Blockages
- It takes a lot of time to test changes due to the build time
- There aren’t a lot of documentation and conversations by other developers about most errors apart from the information from the official Debian wikis which makes debugging a lot more time consuming
- Thanks to my mentors for their availability. I no longer have to worry when I can't find what I am looking for

##31.01.23
###To Do 
- Resolve the connection error with probably ‘apt-cache-ng’
- Testing out the sbuild on at least 10 other random packages to see whether I will get different errors

### Tasks accomplished 
- I was able to test out the sbuild on different packages and here are the findings
- Build passed on my initial test package
- Build failed on the original Salsa-CI test package with this error yet dh-python is already included as a dependency under debian/control
- Build failed on rdate package with this error  
- Build for rapiddisk failed with this error.

### Blockages
- Tested out 3 packages and looking at the errors so far with the first packages, this comment thread seems to highlight the issue of why I was not successfully making builds.

santiago-gsoc@debian.org advised that I used mmdebstrap instead of sbuild-debian-developer-setup since it gives more optimization capabilities. 

#### MY NOTES: 
(after the meeting with Santiago, I was forced to (re)read some things to first understand them better before I go with the implementation)
Packaging requires requires a source package that provides all the required files to compile or build a software. It consists of a;
The upstream tarball with .tar.gz ending
A description file with .dsc ending.
A tarball (with a .debian.tar.gz (source format : 3.0) or a .diff.gz ending (source format : 1.0))

	To get a source package with apt-get ->
$ apt-get source <package name>
$ echo "deb-src http://deb.debian.org/debian/ unstable main" >> /etc/apt/sources.list ### creates a deb-src entry of unstable in /etc/apt/sources.list
Anothe way to package with sbuild would be using the steps described here

Wed, 1st Feb - Fri 3rd
To Do
Make a build with mmdebstrap

Tasks Accomplished
After alterations from s-d-d-build to mmdebstrap, I encountered  this error several times during the different builds. In an attempt to solve the error, I diverted from using mmdebstrap to setting up sbuild manually as of this commit. The commit changes produced different errors that showed progress until this error which on researching, I realised the build was not installing some dependencies according to this explanation.

Blockages
One of the unique blockages this time round was interpreting errors and being able to look for the right documentation to address them. 

Week 6 (6th Feb - 10th Feb)
After several attempts with the three sbuild setups, Focus this week is going to be put on building around the most successful implementation of the three. 
Sbuild Debian Developer Setup - this had a successful build but there where conversations around it’s customization so we opted to try out the other different methods
mmdebstrap - This failed with a consistent error 
Manual setup - there where two attempts with this method, billy warren’s and santiago-gsoc@debian.org’s. The latter implementation proved that this method is also possible.

Mon, 6th
To Do
Focus on sbuild’s manual setup and how it can be integrated into Salsa

>>>>>>>>>>>>>>>RECORDS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
Successful build with different implementations
With sbuild Debian developer setup
With sbuild-createchroot
>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Tue, 7th
To Do
Refining the sbuild-createchroot implementation by trying it out on random packages

Tasks accomplished
Failed on rapiddisk
Rdate package also throws a similar error to the previous package’s latest build

